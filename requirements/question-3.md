## What will your input and output be? 
The program is a battleship trainer. The user will input the positions on the board to hit. The program will output if the inputted position hit a ship, or not, if destroyed a ship, and if the user destroyed all the ships, in that case the user won.  
## Where will you use a list?
A list will be used for the positions on the board. There will be one for the enemy ships, and one for what is printed to the user. They will be nested lists, with each row being a list, so it makes it easier to work with.
## Does your function have sequencing, selection, and iteration?
Yes, there will be a function that takes in a position on the board, and returns if there is something on it. The function will be used while randomly placing the ships. It will check if the ship is on something, if it is, the program will repeat, and it will try a new random position.
## Do you call your function more than once with different output depending on the input?
Yes, the function is also called to check if the position for the start of the ship is taken, and if the positions for the rest of the ship is taken.
