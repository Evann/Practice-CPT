import random

tries = 0
sunken_ships = 0

print_ships = [["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"]]

ships = [["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"],
         ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"], ["-", "-", "-", "-", "-", "-", "-", "-", "-", "-"]]

positions = (("a1", "a2", "a3", "a4", "a5", "a6", "a7", "a8", "a9", "a10"),
             ("b1", "b2", "b3", "b4", "b5", "b6", "b7", "b8", "b9", "b10"),
             ("c1", "c2", "c3", "c4", "c5", "c6", "c7", "c8", "c9", "c10"),
             ("d1", "d2", "d3", "d4", "d5", "d6", "d7", "d8", "d9", "d10"),
             ("e1", "e2", "e3", "e4", "e5", "e6", "e7", "e8", "e9", "e10"),
             ("f1", "f2", "f3", "f4", "f5", "f6", "f7", "f8", "f9", "f10"),
             ("g1", "g2", "g3", "g4", "g5", "g6", "g7", "g8", "g9", "g10"),
             ("h1", "h2", "h3", "h4", "h5", "h6", "h7", "h8", "h9", "h10"),
             ("i1", "i2", "i3", "i4", "i5", "i6", "i7", "i8", "i9", "i10"),
             ("j1", "j2", "j3", "j4", "j5", "j6", "j7", "j8", "j9", "j10"))

lenships = [2, 2, 2, 2, 3, 3, 3, 4, 4, 6]

def is_taken(y_pos, x_pos):
   try:
       ships[y_pos][x_pos]
   except:
       return True
   if ships[y_pos][x_pos] != '-':
       return True
   else:
       return False

def flat(lis):
    flatList = []
    for element in lis:
        if type(element) is list:
            for item in element:
                flatList.append(item)
        else:
            flatList.append(element)
    return flatList


lenships = [2, 3, 3, 4, 5]
ship_letters = ['A', 'B', 'C', 'D', 'E']

for i in range(5):
  while True:
       add_pos = []
       start = [random.randint(0, 9), random.randint(0, 9)]
       if is_taken(start[0], start[1]): continue
       add_pos.append(start)
       vertical = random.choice((True, False))
       for o in range(lenships[i]):
           if vertical:
             new_pos = [start[0]+o, start[1]]
           else: new_pos = [start[0], start[1]+o]
           if is_taken(*new_pos):
               add_pos = []
               break
           add_pos.append(new_pos)
       if not add_pos: continue
       for o in add_pos:
           ships[o[0]][o[1]] = ship_letters[i]
       break

'''
counter = 1
print("   a b c d e f g h i j")
for a in ships:
    if (counter != 10):
        print(f" {counter} {' '.join(a)}")
    else:
        print(f"{counter} {' '.join(a)}")
    counter += 1
print('')
'''

while True:
  counter = 1
  print("   a b c d e f g h i j")
  for a in print_ships:
      if (counter != 10):
          print(f" {counter} {' '.join(a)}")
      else:
          print(f"{counter} {' '.join(a)}")
      counter += 1
  ask = True
  while ask:
    answer = input('\nType a position to hit: ')
    for i in positions:
      try: i.index(answer.lower())
      except:
        ask = True
        continue
      answer_list = [i.index(answer.lower()), positions.index(i)]
      ask = False
      if print_ships[answer_list[0]][answer_list[1]] != '-':
        print('\nYou already shot there')
        ask = True
      break
  tries += 1
  if ships[answer_list[0]][answer_list[1]] == '-':
    print('\nMiss\n')
    print_ships[answer_list[0]][answer_list[1]] = 'o'
  else:
    print('\nHit\n')
    print_ships[answer_list[0]][answer_list[1]] = 'x'
    ships[answer_list[0]][answer_list[1]] = 'x'
  for i in ship_letters:
    try: flat(ships).index(i)
    except:
      print('A ship has sunk\n')
      sunken_ships += 1
      ship_letters.remove(i)
  if sunken_ships == 5:
    print(f'You won in {tries} tries')
    break
