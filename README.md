# Practice-CPT
Practice Create Performance Task
## How To Play Battleship
run this command to download the code:\
`curl https://codeberg.org/Evann/Practice-CPT/raw/branch/main/battleship/main.py -o battleship.py`

run `battleship.py` using python 3

Enter the position you want to hit\
Example input: `a1`

The program will stop once you win

## View Enemy Ships
open `main.py` in a text editor

uncomment this code at lines 75-85 by removing the `'''` from the top and bottom:
```
'''
counter = 1
print("   a b c d e f g h i j")
for a in ships:
    if (counter != 10):
        print(f" {counter} {' '.join(a)}")
    else:
        print(f"{counter} {' '.join(a)}")
    counter += 1
print('')
'''
```
now when you run `main.py`, it will print the enemy ships
